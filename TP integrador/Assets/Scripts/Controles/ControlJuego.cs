﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public Text textoPuntos;
    public Text textoPerdiste;
    public int cont;
    

    private void Start()
    {        
        setearTextos();
        cont = 0;
        textoPerdiste.text = "";
        if (GestorPersistencia.instancia.Persistencia == true)
        {
            jugador.transform.position = new Vector3(GestorPersistencia.instancia.data.posicionJugador.x, 
                GestorPersistencia.instancia.data.posicionJugador.y,
                GestorPersistencia.instancia.data.posicionJugador.z);

        }
    }
    private void setearTextos()
    {
        // Texto en pantalla y la cantidad de recolectados

        textoPuntos.text = "Puntos: " + cont.ToString();
        
    }

    private void OnTriggerEnter(Collider other)
    {
        {
            if (other.gameObject.CompareTag("coleccionable") == true)
            {
                cont = cont + 1;
                setearTextos();
                other.gameObject.SetActive(false);
            }

        }
    }
    public void Update()
    {
            if (Input.GetKeyDown(KeyCode.G))
            {

            GestorPersistencia.instancia.data.posicionJugador = new Punto(jugador.transform.position);
                GestorPersistencia.instancia.GuardarData();

            }        
    }
}





