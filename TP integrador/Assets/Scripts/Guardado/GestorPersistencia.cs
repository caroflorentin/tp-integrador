﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public class GestorPersistencia : MonoBehaviour
{
    public static GestorPersistencia instancia;
    public DataPersistencia data;
    string saveData = "save.dat";    
    public bool Persistencia = false;


    private void Awake()
    {
        if (instancia is null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        CargarData();

    }

    void Start()
    {

    }


   
    

    public void GuardarData()
    {


        

        string filePath = Application.persistentDataPath + "/" + saveData;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");




    }

    public void CargarData()
    {
        string filePath = Application.persistentDataPath + "/" + saveData;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;           
            file.Close();
            Debug.Log("Datos cargados");
            Persistencia = true;
        }

    }

}

[System.Serializable]

public class DataPersistencia
{

    public Punto posicionJugador;





}


[System.Serializable]

public class Punto
{

    public float x;
    public float y;
    public float z;


    public Punto(Vector3 p)
    {
        x = p.x;
        y = p.y;
        z = p.z;
    }


    public Vector3 aVector()
    {
        return new Vector3(x, y, z);
    }



}