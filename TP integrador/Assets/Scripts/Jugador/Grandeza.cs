﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grandeza : MonoBehaviour
// QUE CAMBIE DE TAMAÑO
{
    bool grandeza;
    public GameObject Jugador;

    private void OnTriggerEnter(Collider other)

    {
        
        if (other.gameObject.CompareTag("grandeza") == true)
        {           
            other.gameObject.SetActive(false);
            grandeza = true;
            GestorDeAudio.instancia.ReproducirSonido("mario grande");
            Jugador.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
            
        }
    }
}
