﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SumaVida : MonoBehaviour
{
    public float vida = 200;

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetComponent<SistemaVida>() != null)

        {
            other.gameObject.GetComponent<SistemaVida>().DarVida(200);
            {
                if (other.gameObject.CompareTag("vida") == true) 
                {
                    GestorDeAudio.instancia.ReproducirSonido("vida");
                    other.gameObject.GetComponent<SistemaVida>().DarVida(200);
                }
            }
        }
    }
}
