﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigos : MonoBehaviour
{    
    public float daño;
    public float vida;
    public GameObject enemigo;   
    void Start()
    {        
        vida = 150;
    }

    public void recibirDaño()
    {
        vida -= 30; 
        if (vida <= 0)
        {
            Muerte();
        }
    }
   
   public void Muerte()
    {
       Destroy(gameObject);       
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("bala") == true)
        {
            recibirDaño();
        }
    }
}