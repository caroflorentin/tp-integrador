﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Colisiones : MonoBehaviour
{
    public int rapidez;
    bool habilidadRapidez;
    bool elevarse;
    public int cont;
    public float daño = 100;
    public Text textoPuntos;
    public Rigidbody rb;
    public GameObject Jugador;
    public ParticleSystem Confetti;
    public GameObject BalaInicio;
    public GameObject BalaPrefab;
    public float BalaVelocidad;
    public bool Disparo;
    
    void OpenParachute()
    {
        rb.drag = 20;
    }
    private void Start()
    {
        rapidez = GameObject.Find("Personaje").GetComponent<ControlJugador>().rapidez;
        rb = GameObject.Find("Personaje").GetComponent<Rigidbody>();
        Disparo = false; 
    }
    private void OnTriggerEnter(Collider other)

    {
        if (other.gameObject.CompareTag("disparar") == true)
        {
            GestorDeAudio.instancia.PausarSonido("estrella");

            other.gameObject.SetActive(false);
            //if (Input.GetKeyDown(KeyCode.Q))
            //{                
            //    GameObject BalaTemporal = Instantiate(BalaPrefab, BalaInicio.transform.position, BalaInicio.transform.rotation) as GameObject;                 
            //    Rigidbody rb = BalaTemporal.GetComponent<Rigidbody>();                
            //    rb.AddForce(transform.forward * BalaVelocidad);                
            //    Destroy(BalaTemporal, 5.0f);
            Disparo = true;
            //}
        }
        if (other.gameObject.CompareTag("Portal") == true)
        {            
            Jugador.transform.position = new Vector3(-64, -7, -1);
            other.gameObject.SetActive(false);
            GestorDeAudio.instancia.PausarSonido("musica");
            GestorDeAudio.instancia.ReproducirSonido("arboles");
        }
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            other.gameObject.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("monedita");
        }
                   
            if (other.gameObject.CompareTag("elevarse") == true)
            {
                other.gameObject.SetActive(false);                
                GestorDeAudio.instancia.ReproducirSonido("elevarse");
                rb.drag = 5;
                rb.mass = 0.2f;
            }                              
        {
            // Que suba la rapidez al agarrar la habilidad.

            if (other.gameObject.CompareTag("habilidadRapidez") == true)
            {
                other.gameObject.SetActive(false);
                habilidadRapidez = true;
                GestorDeAudio.instancia.ReproducirSonido("estrella");
            }
            if (habilidadRapidez)
            {
                rapidez = 15;
                rb.drag = 0;
                rb.mass = 1;
            }
        }
        {
            if (other.gameObject.CompareTag("meta") == true)
            {
                GestorDeAudio.instancia.ReproducirSonido("mario gana");
                other.gameObject.SetActive(false);
                habilidadRapidez = true;                
                Confetti.Play();
            }
        }
        {
            if (other.gameObject.CompareTag("coleccionable") == true)
            {
                cont = cont + 1;
                setearTextos();
                other.gameObject.SetActive(false);
            }
        }
        {
            //{
            //    if (other.gameObject.GetComponent<VidaEnemigos>() != null)

            //    {
            //        other.gameObject.GetComponent<VidaEnemigos>().QuitarVida(daño);

            //    }
            //}
            
        }
        
    }
    public void setearTextos()
    {
        textoPuntos.text = "Puntos: " + cont.ToString();
    }
}