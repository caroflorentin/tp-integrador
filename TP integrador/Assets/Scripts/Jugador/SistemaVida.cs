﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SistemaVida : MonoBehaviour
{
    public float vidaMaxima = 150.0f;
    public float vidaActual;
    public float daño;
    public float vida;
    public GameObject player;
    public Vector3 posicion;
    public Text textoPerdiste;
    void Start()
    {
        posicion = new Vector3(39, 6, -1);
        vidaActual = vidaMaxima;
        setearTexto();
        textoPerdiste.text = "";
    }

    private void Update()
    {
        if (vidaActual > vidaMaxima)
        {
            vidaActual = vidaMaxima;
        }

        if (vidaActual <= 0)

        {
            Muerte();                       
            textoPerdiste.text = "Perdiste! R para reiniciar";
            
        }
        
    }
    private void setearTexto()
    {
        if (vidaActual <= 0)

        {
            Muerte();
            textoPerdiste.text = "Perdiste! R para reiniciar";

        }
    }
        public void QuitarVida(float daño)
    {
        vidaActual -= daño;
    }

    public void DarVida(float vida)
    {
        vidaActual += vida;
    }

    public void Muerte()
    {
        player.SetActive(false);
        player.transform.position = new Vector3(); 

    }
}
