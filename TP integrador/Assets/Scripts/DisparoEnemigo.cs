﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{
    public GameObject Jugador;
    private float cooldown = 4;
    private float disparo = 0;    
    public Camera camFps;
    Rigidbody rb;
    public GameObject bala;
    private void Update()
    {
        transform.LookAt(Jugador.transform);
        if (Time.time > disparo)
        {
            disparo = Time.time + cooldown;
            Ray rayo = camFps.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(bala, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camFps.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 3);

        }
    }
}
