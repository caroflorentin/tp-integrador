﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPiedra : MonoBehaviour

{
    
    public GameObject jugador;
    public int rapidez;

   

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}
