﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public int rapidez;
    public float magnitudSalto;
    public LayerMask enPiso;
    private Rigidbody rb;
    public CapsuleCollider col;       
    

    //void OpenParachute()
    //{
    //    rb.drag = 20;
    //}
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        GestorDeAudio.instancia.ReproducirSonido("musica");             
    }

    private void FixedUpdate()
    {
        //MOVIMIENTO JUGADOR

       
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;

        movimientoAdelanteAtras *= Time.deltaTime;
        

        transform.Translate(0, 0, movimientoAdelanteAtras);
        
        
        //    if (rapidez == 10f)
        // {
        //    rb.drag = 5;
        //    rb.mass = 0.2f; 
        //}
        //    else
        //{
        //    rb.drag = 0;
        //    rb.mass = 1;
        //}
    }
    

    private void Update()
    //SALTO
    {
        if (Input.GetKeyDown(KeyCode.Space) && estaEnPiso())
        {
            GestorDeAudio.instancia.ReproducirSonido("salto");
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            
        }        
        // REINICIO
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            if (this.transform.position.y <= -13)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

            }
            
        }
     }
    private bool estaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, enPiso);

    }   
} 

    




