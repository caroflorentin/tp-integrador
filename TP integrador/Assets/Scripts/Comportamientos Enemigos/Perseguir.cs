﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Perseguir : MonoBehaviour
{
    private int hp;
    private GameObject jugador;
    public int rapidez;
    private Rigidbody rb;
    private float gravityScale = 1.0f;
    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Jugador");
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void Oneable()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false; 
    }

}
