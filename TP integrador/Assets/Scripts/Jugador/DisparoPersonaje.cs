﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoPersonaje : MonoBehaviour
{

    public GameObject BalaInicio;
    public GameObject BalaPrefab;
    public float BalaVelocidad;
    public GameObject Jugador;
    private float cooldown = 0.5f;
    private float disparo;

    void Update()
    {
        if (Time.time > disparo)
        {


            if (Jugador.GetComponent<Colisiones>().Disparo == true)
            {

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    disparo = Time.time + cooldown;
                    GameObject BalaTemporal = Instantiate(BalaPrefab, BalaInicio.transform.position, BalaInicio.transform.rotation) as GameObject;
                    Rigidbody rb = BalaTemporal.GetComponent<Rigidbody>();
                    rb.AddForce(transform.forward * BalaVelocidad);
                    Destroy(BalaTemporal, 5.0f);
                }
            }
        }
    }
}
